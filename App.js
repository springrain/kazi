
import { Navigation } from 'react-native-navigation';
import {Provider} from 'react-redux';

import Login from './src/screen/login/Login'
import MyBalance from './src/screen/myBalance/MyBalance'



//create store and 
import configureStore from './src/store/configureStore';
const store = configureStore();

// register some screen and pass store and provider to connect redux in app
Navigation.registerComponent("myApp.Login", () => Login, store, Provider);
Navigation.registerComponent("myApp.MyBalance", () => MyBalance, store, Provider);

// start app
Navigation.startSingleScreenApp({
  screen: {
    screen: "myApp.Login",
    title: "Login"
  }
});
