import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground
} from "react-native";


const listItem = props => (
  <TouchableOpacity>
      <View style={styles.listItem}>
        <Text style={styles.tokenStyle}>{"A/C No: " + props.token}</Text>
        <View style={styles.lineStyle}></View>
        <Text style={styles.textHeading}>{props.tokenType}</Text>
        <Text style={styles.tokenStyle}>{props.currency+" " +props.balance}</Text>
      </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  listItem: {
    width: "100%",
    marginBottom: 20,
    padding: 10,
    alignItems: "center",
    backgroundColor:"#3F51B5",
    borderColor: "#fff",
    borderWidth: 5,
  },
  placeImage: {
    marginRight: 8,
    height: 30,
    width: 30
  },
  tokenStyle: {
    fontSize: 18,
    color:"#fff"
  },
  backgroundImage: {
    width: "100%",
    flex: 1,
    margin:10
  },lineStyle:{
      backgroundColor:"#ffff",
      width:"100%",
      height:2,
      marginTop:10,
      marginBottom:10
  },textHeading: {
    fontSize: 24,
    fontWeight: "bold",
    color:"#fff"
  }
});

export default listItem;