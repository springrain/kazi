import React from "react";
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  Text,
  View,
  StyleSheet,
  Platform
} from "react-native";

const buttonWithBackground = props => {
  const content = (
    // <TouchableOpacity>
      <View style={[style.button, { backgroundColor: props.color }]}>
        <Text style={style.text}>{props.children}</Text>
      </View>
    // </TouchableOpacity>
  );

  if (Platform.OS == "android") {
    return (
      <TouchableWithoutFeedback onPress={props.onPress}>
        {content}
      </TouchableWithoutFeedback>
    );
  } else {
    return (
      <TouchableOpacity onPress={props.onPress}>{content}</TouchableOpacity>
    );
  }
};

const style = StyleSheet.create({
  button: {
    padding: 10,
    margin: 5,
    alignItems: "center",
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: "#ffffff"
  },
  text: {
    color: "#ffffff"
  }
});

export default buttonWithBackground;
