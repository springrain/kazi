import { LOGIN ,SAVE_RESPONSE_DATA ,TOKEN_RESPONSE} from "../actions/actionTypes";

const initialState = {
  loginUserData: {}, 
  tokenResult:[]
 
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return{
        ...state
      } 

    case SAVE_RESPONSE_DATA: 
    return {
      ...state, 
      loginUserData: action.result
    }
    case TOKEN_RESPONSE:
    return {
      ...state,
      tokenResult: action.tokenResult
    };

    default:
      return state;
  }
};

export default reducer;
