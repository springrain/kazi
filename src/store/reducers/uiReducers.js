import {
  IS_LOGIN,
  LOGOUT,
  UI_START_LOADING,
  UI_STOP_LOADING,
  UI_LOGIN_FAIL,
  CLEAR_ERROR_MESSAGE
} from "..//actions/actionTypes";

const initialState = {
  isLoading: false,
  errorMessage: '', 
  isLogged: false
};

const reducerName = (state = initialState, action) => {
  switch (action.type) {
    case UI_START_LOADING:
      return {
        ...state,
        isLoading: true
      };

    case UI_STOP_LOADING:
      return {
        ...state,
        isLoading: false
      };

    case UI_LOGIN_FAIL:
      return {
        ...state,
        errorMessage: action.errorMessage
      };
    case CLEAR_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: ''
      };

      case IS_LOGIN:
      return {
        ...state,
        isLogged: true
      };

      case LOGOUT:
      return {
        ...state,
        isLogged: false
      };

    default:
      return state;
  }
};

export default reducerName;
