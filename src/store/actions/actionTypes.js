export const LOGIN = 'LOGIN';
export const SAVE_RESPONSE_DATA = 'SAVE_RESPONSE_DATA';





// All action for UI 
export const UI_START_LOADING = 'UI_START_LOADING';
export const UI_STOP_LOADING = 'UI_STOP_LOADING';


export const UI_LOGIN_FAIL = 'UI_LOGIN_FAIL';
export const UI_MOVE_NEXT_PAGE = 'UI_MOVE_NEXT_PAGE';

export const CLEAR_ERROR_MESSAGE = "CLEAR_ERROR_MESSAGE";export const IS_LOGIN = 'IS_LOGIN';

export const LOGOUT = 'LOGOUT';


// all action for my balance 

export const GET_ALL_TOKEN_LIST = 'GET_ALL_TOKEN_LIST';



export const TOKEN_REQUEST='TOKEN_REQUEST';
export const TOKEN_RESPONSE='TOKEN_RESPONSE';
export const BALANCE_CHECK='BALANCE_CHECK';
export const BALANCE_RESPONSE='BALANCE_RESPONSE';