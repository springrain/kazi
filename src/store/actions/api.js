// API action creator
import {SAVE_RESPONSE_DATA,TOKEN_REQUEST,TOKEN_RESPONSE } from "./actionTypes"
import { isLogged, uiStartLoading, uiStopLoading, uiLoginFail} from './index'


export const doLogin = (userName, password) => {
  return dispatch => {
     // show loading in ui 
     dispatch(uiStartLoading());

    // create a header
    const header = {
      ApiUser: "ws2-mobile",
      ApiKey: "WJDtSxWPCZenceHrQvJFrE9AI5SDxXAwBnjAaFik9A0=",
      "Content-Type": "application/json"
    };
    
    // create a request body 
    const requestBody = {
      UserName: userName,
      Password: password
    };
    //  const url = "https://requestb.in/th4i73th";
     const url = "https://stage.aucorpws.com/webservice2.0/user/login";
    // call a post request 
    fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(requestBody)
    })
    .catch(err =>{
        console.log(err);
        dispatch(uiStopLoading());
        dispatch(uiLoginFail(err.message));
    })
    .then(res =>res.json())
    .then(parsedRes =>{
        console.log(parsedRes);
        dispatch(uiStopLoading());
        if(parsedRes.status !== 0){
          
          dispatch(uiLoginFail(parsedRes.message));
        }else{
          dispatch(saveLoginResponseData(parsedRes.result));
          dispatch(isLogged());
        }
    });
  };

  }


 export const saveLoginResponseData=loginResponse=>{
    console.log(loginResponse);
    return {
      type: SAVE_RESPONSE_DATA,
      result: loginResponse
    };
  };


  export const getTokenList = tokenData => {
    return dispatch => {
      
      dispatch(uiStartLoading());

      fetch("https://stage.aucorpws.com/webservice2.0/user/get/token/list", {
        method: "POST",
        body: JSON.stringify(tokenData),
        headers: {
          ApiUser: "ws2-mobile",
          ApiKey: "WJDtSxWPCZenceHrQvJFrE9AI5SDxXAwBnjAaFik9A0=",
          "Content-Type": "application/json"
        }
      })
        .catch(err =>{
            dispatch(uiStopLoading());
        })
        .then(res => res.json()) 
        .then(parsedRes => {
          if(parsedRes.status===0){
            const tokenList=[];
            for(let key in parsedRes.result){
              tokenList.push({
                ...parsedRes.result[key],
                key:key
              })
            }
            dispatch(uiStopLoading());
            dispatch(setTokenResponse(tokenList));
          }
          console.log(parsedRes);
        });
    };
  };
  
  export const setTokenResponse=tokenList=>{
    console.log(tokenList);
    return {
      type: TOKEN_RESPONSE,
      tokenResult:tokenList
    };
  };


