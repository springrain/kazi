import {IS_LOGIN,LOGOUT,UI_START_LOADING, UI_STOP_LOADING, UI_LOGIN_FAIL, UI_MOVE_NEXT_PAGE, CLEAR_ERROR_MESSAGE} from './actionTypes'


export const uiStartLoading = () =>{
    return {
        type: UI_START_LOADING
    }
}

export const uiStopLoading = () =>{
    return {
        type: UI_STOP_LOADING
    }
}

export const uiLoginFail = (errorMessage) =>{
    return {
        type: UI_LOGIN_FAIL,
        errorMessage : errorMessage
    }
}


export const clearErrorMessage = () =>{
    return {
        type: CLEAR_ERROR_MESSAGE
    }
}


  export const isLogged = () =>{
    return {
        type: IS_LOGIN
    }
  }

  export const logout = () =>{
    return {
        type: LOGOUT
    }
  }
  