const validate = (val, rules) =>{
   let isValid = true;
   for (let rule in rules){
       switch(rule){
           case 'isUserName':
           isValid = isValid && userNameValidator(val);
           break;
           case 'password':

           isValid = isValid && passwordValidator (val);
           break;
           default: 
           isValid = true;

       }
   } 
}

const userNameValidator = val =>{
    return val.length > 1;

}

const passwordValidator = val =>{
    return val.length > 1;

}