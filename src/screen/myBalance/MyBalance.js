import React, { Component } from "react";
import { Navigation } from 'react-native-navigation';

import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Platform,
  FlatList
} from "react-native";
import { connect } from "react-redux";
import { getTokenList } from "../../store/actions/index";
import ListItem from "../../components/ListItem/ListItem";
import ButtonWithBackground from ".//..//.//..//components/ui/buttonWithBackground/ButtonWithBackgroud";

import {
  Dialog,
  ProgressDialog,
  ConfirmDialog
} from "react-native-simple-dialogs";

import { logout } from "..//..//store/actions/index";

import createStore from "../../store/configureStore";
const store = createStore();

class MyBalance extends Component {

  static navigatorButtons = {
    rightButtons: [
      {
        title: "Logout",
        id: "logout",
        showAsAction: "always",
        buttonColor: "#3F51B5",
        buttonFontSize: 16,
        buttonFontWeight: "600"
      }
    ]
  };


  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  onNavigatorEvent(event) {
    if (event.type == "NavBarButtonPress") {
      if (event.id == "logout") {
        Navigation.startSingleScreenApp({
          screen: {
            screen: "myApp.Login",
            title: "Login"
          }
        });
      }
    }
  }


  componentDidMount() {
    this.props.onLogout();

    const tokenData = {
      UserId: "31"
    };

    this.props.getToken(tokenData);
  }
  render() {
    return (
      <View>
        <TouchableOpacity>
          <View style={styles.headerText}>
            <Text style={styles.textHeading}>{this.props.result.first_name +" "+this.props.result.last_name}</Text>
          </View>
        </TouchableOpacity>
        <FlatList
          style={styles.listContainer}
          data={this.props.tokenResult}
          renderItem={info => (
            <ListItem
              token={info.item.token}
              currency={info.item.currency}
              tokenType={info.item.token_type}
              balance={info.item.available_balance}
            />
          )}
        />

         {/* <View style={styles.loginButton}>
          <ButtonWithBackground
            color="#3F51B5"
            onPress={() => this.callLoginAPI()}
          >
            LOGOUT
          </ButtonWithBackground>
        </View> */}

        <ProgressDialog
          visible={this.props.isLoading}
          message="Please, wait..."
          animationType="slide"
          activityIndicatorSize="large"
          activityIndicatorColor="blue"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    backgroundColor: "white",
    flex: 1,
    width: "100%"
  },
  headerText: {
    alignItems: "center",
    width: "100%",
    padding: 10,
    backgroundColor: "#eee"
  },
  drawerItemIcon: {
    marginRight: 10
  },
  listContainer: {
    width: "100%",
    marginRight: 20
  },
  textHeading: {
    fontSize: 28,
    fontWeight: "bold"
  }
  , loginButton: {
    width: "80%"
  }
});

const mapStateToProps = state => {
  return {
    result: state.API.loginUserData,
    tokenResult: state.API.tokenResult
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => dispatch(logout()),
    getToken: tokenData => dispatch(getTokenList(tokenData))
  };
};




export default connect(mapStateToProps, mapDispatchToProps)(MyBalance);
