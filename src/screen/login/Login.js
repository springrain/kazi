import React, { Component } from "react";
import {
  View,
  Text,
  Button,
  TextInput,
  Image,
  StyleSheet,
  ImageBackground
} from "react-native";
import { connect } from "react-redux";

import {
  doLogin,
  isLogged,
  uiStartLoading,
  uiStopLoading,
  clearErrorMessage
} from "..//..//store/actions/index";

import DefaultInput from ".//..//.//..//components/ui/defaultInput/DefaultInput";
import ButtonWithBackground from ".//..//.//..//components/ui/buttonWithBackground/ButtonWithBackgroud";
import Logo from ".//..//../assets/logo.png";

import validate from "..//..//utility/validation";

import {
  Dialog,
  ProgressDialog,
  ConfirmDialog
} from "react-native-simple-dialogs";

export class LoginScreen extends Component {
  componentDidUpdate() {
    //login success
    if (this.props.isLogged) {
      this.showMyBalancePage();
    }
  }

  state = {
    isShowTest: true,
    controls: {
      userName: {
        value: ""
      },
      password: {
        value: ""
      }
    }
  };

  showMyBalancePage = () => {
    this.props.navigator.resetTo({
      screen: "myApp.MyBalance"
    });
  };

  openProgress = () => {
    this.setState({ showProgress: true });
  };

  closeProgress = () => {
    this.setState({ showProgress: false });
  };

  callLoginAPI = () => {
    this.props.onLogin(
      this.state.controls.userName.value.toLowerCase(),
      this.state.controls.password.value.toLocaleLowerCase()
    );
  };

  openDialog(show) {
    this.setState({ showDialog: show });
  }

  submitButtonHandler() {
    console.log("user Name : " + this.state.controls.userName.value);
    console.log(
      "user Name length : " + this.state.controls.userName.value.length
    );

    console.log("user password : " + this.state.controls.password.value);
    console.log(
      "user password length : " + this.state.controls.password.value.length
    );

    if (
      this.state.controls.userName.value.length > 0 &&
      this.state.controls.password.value.length > 0
    ) {
      console.log("valid user name and password");
      this.callLoginAPI();
    } else {
      console.log();
      alert("Invalid user name and password");
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={Logo} style={styles.logo} />
        <View style={styles.inputContainer}>
          <DefaultInput
            placeholder="User name"
            //  value={this.state.controls.userName.value}
            onChangeText={val => {
              this.state.controls.userName.value = val;
            }}
          />
          <DefaultInput
            secureTextEntry = {true}
            placeholder="Password"
            // value={this.state.controls.password.value}
            onChangeText={val => {
              this.state.controls.password.value = val;
            }}
            
            
            
          />
        </View>
        <View style={styles.loginButton}>
          <ButtonWithBackground
            color="#3F51B5"
            onPress={() => this.submitButtonHandler()}
          >
            {" "}
            LOGIN
          </ButtonWithBackground>
        </View>

        <ProgressDialog
          visible={this.props.isLoading}
          message="Please, wait..."
          animationType="slide"
          activityIndicatorSize="large"
          activityIndicatorColor="blue"
        />

        <ConfirmDialog
          title="Message"
          message={this.props.errorMessage}
          visible={this.props.errorMessage !== ""}
          onTouchOutside={() => this.setState({ dialogVisible: false })}
          positiveButton={{
            title: "Ok",
            onPress: () => {
              this.props.onClearErrorMessage();
            }
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFFFFF",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    width: 200,
    height: 200
  },
  inputContainer: {
    width: "80%"
  },
  loginButton: {
    width: "80%"
  }
});

const mapStateToProps = state => {
  return {
    isLoading: state.uiReducer.isLoading,
    isLogged: state.uiReducer.isLogged,
    errorMessage: state.uiReducer.errorMessage
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: (userName, password) => dispatch(doLogin(userName, password)),
    onClearErrorMessage: () => dispatch(clearErrorMessage())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
// export default LoginScreen;
