import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import renderer from "react-test-renderer";

import ListItem from ".//../../src/components/ListItem/ListItem";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground
} from "react-native";

configure({ adapter: new Adapter() });

describe("test ListItem", () => {
  let component;
  let textInput;
  const defaultState = { text: "" };

  test("renders 3 Text have ", () => {
    const wrapper = shallow(<ListItem/>);
    expect(wrapper.find(Text)).toHaveLength(3);
  });

  test("renders 3 Text has ", () => {
    const wrapper = shallow(<ListItem/>);
    expect(wrapper.find(Text)).toHaveLength(3);
  });

  test("A/C No: text label has", () => {
    const wrapper = shallow(<ListItem />);
    expect(wrapper.first(Text)).text === "A/C No: ";
  });

});
