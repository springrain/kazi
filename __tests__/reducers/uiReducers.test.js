import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import renderer from "react-test-renderer";
import uiReducers from ".//.//..//../src/store/reducers/uiReducers";
import { UI_START_LOADING , UI_STOP_LOADING, UI_LOGIN_FAIL, CLEAR_ERROR_MESSAGE, IS_LOGIN, LOGOUT} from "..//..//src/store/actions/actionTypes";

describe("UI reducers", () => {
  it("default action and return  initialState", () => {
    expect(
      uiReducers(
        {
          isLoading: false,
          errorMessage: "",
          isLogged: false
        },
        "any"
      )
    ).toEqual({
      isLoading: false,
      errorMessage: "",
      isLogged: false
    });
  });

  it("given UI_START_LOADING action and return  isLoading = true ", () => {
    expect(uiReducers(
      undefined,
      {
        type: UI_START_LOADING,
        isLoading: true
      }
    )).toEqual({
      isLoading: true,
      errorMessage: "",
      isLogged: false
    });
  });
  it("given UI_STOP_LOADING action and return  isLoading = false ", () => {
    expect(uiReducers(
      undefined,
      {
        type: UI_STOP_LOADING,
        isLoading: false
      }
    )).toEqual({
      isLoading: false,
      errorMessage: "",
      isLogged: false
    });
  });


  it("given UI_LOGIN_FAIL action and return  error message", () => {
  
  expect(uiReducers(
      undefined,
      {
        type: UI_LOGIN_FAIL,
        errorMessage: 'user not found'
      }
    )).toEqual({
      isLoading: false,
      errorMessage: 'user not found',
      isLogged: false
    });
  });


  it("given CLEAR_ERROR_MESSAGE action and return clear error message ", () => {
  
    expect(uiReducers(
        undefined,
        {
          type: CLEAR_ERROR_MESSAGE,
          errorMessage: ''
        }
      )).toEqual({
        isLoading: false,
        errorMessage: '',
        isLogged: false
      });
    });


    it("given IS_LOGIN action and return clear isLogged = true ", () => {
  
        expect(uiReducers(
            undefined,
            {
              type: IS_LOGIN,
              isLogged: true
            }
          )).toEqual({
            isLoading: false,
            errorMessage: '',
            isLogged: true
          });
        });


        it("given LOGOUT action and return clear isLogged = true ", () => {
  
            expect(uiReducers(
                undefined,
                {
                  type: LOGOUT,
                  isLogged: false
                }
              )).toEqual({
                isLoading: false,
                errorMessage: '',
                isLogged: false
              });
            });


        

});
