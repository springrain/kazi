import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import renderer from "react-test-renderer";
import apiReducers from "..//..//src/store/reducers/apiReducers";
import {
  LOGIN,
  SAVE_RESPONSE_DATA,
  TOKEN_RESPONSE
} from "..//..//src/store/actions/actionTypes";

describe("api reducers", () => {
  it("default action and return  initialState", () => {
    expect(
      apiReducers(
        {
          loginUserData: {},
          tokenResult: []
        },
        "any"
      )
    ).toEqual({
      loginUserData: {},
      tokenResult: []
    });
  });

  it("given LOGIN SAVE_RESPONSE_DATA action and return to loginUserData", () => {
    expect(
      apiReducers(undefined, {
        type: SAVE_RESPONSE_DATA,
        result: { id: 100 }
      })
    ).toEqual({
      loginUserData: { id: 100 },
      tokenResult: []
    });
  });

  it("given LOGIN TOKEN_RESPONSE action and return to token list", () => {
    expect(
      apiReducers(undefined, {
        type: TOKEN_RESPONSE,
        tokenResult: [1, 2, 3]
      })
    ).toEqual({
      loginUserData: {},
      tokenResult: [1, 2, 3]
    });
  });
});
